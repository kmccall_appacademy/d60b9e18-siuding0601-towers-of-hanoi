class TowersOfHanoi

attr_reader :towers

  def initialize
    @towers = [[3, 2, 1],[],[]]
  end

  def move(from_tower, to_tower)
    towers[to_tower].push(towers[from_tower].pop)
  end

  def valid_move?(from_tower, to_tower)
    if towers[from_tower] == []
      false
    elsif towers[to_tower] != [] && towers[to_tower][-1] < towers[from_tower][-1]
      false
    else
      true
    end

  end

  def won?
    if towers[1] == [3, 2, 1] || towers[2] == [3, 2, 1]
      true
    else
      false
    end
  end

  def render
    puts towers.inspect
  end

  def play
    while self.won? == false
      print "What pile do you want to move the disk from?"
      from_tower = gets.chomp.to_i
      print "Where do you want to move it to?"
      to_tower = gets.chomp.to_i
        if self.valid_move?(from_tower, to_tower)
          self.move(from_tower, to_tower)
        elsif  self.valid_move?(from_tower, to_tower) == false
          puts "Invalid move"
        end
        self.render
    end
    print "You won"
  end

end
# if you want to play, remove comments on the following lines
# game = TowersOfHanoi.new
# game.play
